const processInput = require('./processInput');

describe('processInput', () => {
  it('installs a single dependency', () => {
    const input = `2
INSTALL A
END`;

    const result = processInput(input);

    expect(result).toEqual([
      'INSTALL A',
      'Installing A',
      'END'
    ]);
  });

  it('installs a package with dependencies', () => {
    const input = `4
DEPEND A B C
INSTALL B
INSTALL A
END`;

    const result = processInput(input);

    expect(result).toEqual([
      'DEPEND A B C',
      'INSTALL B',
      'Installing B',
      'INSTALL A',
      'Installing C',
      'Installing A',
      'END'
    ]);
  });

  it('installs a package with dependencies and list', () => {
    const input = `5
DEPEND A B C
INSTALL B
INSTALL A
LIST
END`;

    const result = processInput(input);

    expect(result).toEqual([
      'DEPEND A B C',
      'INSTALL B',
      'Installing B',
      'INSTALL A',
      'Installing C',
      'Installing A',
      'LIST',
      'B',
      'C',
      'A',
      'END'
    ]);
  });

  it('ignores circular dependency', () => {
    const input = `3
DEPEND A B
DEPEND B A
END`;

    const result = processInput(input);

    expect(result).toEqual([
      'DEPEND A B',
      'DEPEND B A',
      'A depends on B, ignoring command',
      'END'
    ]);
  });

  it('removes a package', () => {
    const input = `3
INSTALL A
REMOVE A
END`;

    const result = processInput(input);

    expect(result).toEqual([
      'INSTALL A',
      'Installing A',
      'REMOVE A',
      'Removing A',
      'END'
    ]);
  });

  it('do not install already installed package', () => {
    const input = `3
INSTALL A
INSTALL A
END`;

    const result = processInput(input);

    expect(result).toEqual([
      'INSTALL A',
      'Installing A',
      'INSTALL A',
      'A is already installed',
      'END'
    ]);
  });

  it('pass expected behaviour', () => {
    const input = `22
DEPEND TELNET TCPIP NETCARD
DEPEND TCPIP NETCARD
DEPEND NETCARD TCPIP
DEPEND DNS TCPIP NETCARD
DEPEND BROWSER TCPIP HTML
INSTALL NETCARD
INSTALL TELNET
INSTALL foo
REMOVE NETCARD
INSTALL BROWSER
INSTALL DNS
LIST
REMOVE TELNET
REMOVE NETCARD
REMOVE DNS
REMOVE NETCARD
INSTALL NETCARD
REMOVE TCPIP
REMOVE BROWSER
REMOVE TCPIP
LIST
END`;

    const result = processInput(input);

    expect(result).toEqual([
      'DEPEND TELNET TCPIP NETCARD',
      'DEPEND TCPIP NETCARD',
      'DEPEND NETCARD TCPIP',
      'TCPIP depends on NETCARD, ignoring command',
      'DEPEND DNS TCPIP NETCARD',
      'DEPEND BROWSER TCPIP HTML',
      'INSTALL NETCARD',
      'Installing NETCARD',
      'INSTALL TELNET',
      'Installing TCPIP',
      'Installing TELNET',
      'INSTALL foo',
      'Installing foo',
      'REMOVE NETCARD',
      'NETCARD is still needed',
      'INSTALL BROWSER',
      'Installing HTML',
      'Installing BROWSER',
      'INSTALL DNS',
      'Installing DNS',
      'LIST',
      'NETCARD',
      'TCPIP',
      'TELNET',
      'foo',
      'HTML',
      'BROWSER',
      'DNS',
      'REMOVE TELNET',
      'Removing TELNET',
      'REMOVE NETCARD',
      'NETCARD is still needed',
      'REMOVE DNS',
      'Removing DNS',
      'REMOVE NETCARD',
      'NETCARD is still needed',
      'INSTALL NETCARD',
      'NETCARD is already installed',
      'REMOVE TCPIP',
      'TCPIP is still needed',
      'REMOVE BROWSER',
      'Removing BROWSER',
      'Removing TCPIP',
      'Removing HTML',
      'REMOVE TCPIP',
      'TCPIP is not installed',
      'LIST',
      'NETCARD',
      'foo',
      'END'
    ]);
  });
});
