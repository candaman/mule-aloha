const getPackageDependencies = require('./getPackageDependencies');

describe('getPackageDependencies', () => {
  it('returns empty array if no dependencies', () => {
    const packageName = 'A';
    const dependencies = {};

    const result = getPackageDependencies(packageName, dependencies);

    expect(result).toEqual([]);
  });

  it('returns A dependencies', () => {
    const packageName = 'A';
    const dependencies = { A: ['B', 'C'] };

    const result = getPackageDependencies(packageName, dependencies);

    expect(result).toEqual(expect.arrayContaining(['B', 'C']));
  });

  it('does not return other dependencies', () => {
    const packageName = 'A';
    const dependencies = { X: ['B', 'C'] };

    const result = getPackageDependencies(packageName, dependencies);

    expect(result).toEqual([]);
  });

  it('returns A and B dependencies (level 1 transitive)', () => {
    const packageName = 'A';
    const dependencies = { A: ['B'], B: ['C'] };

    const result = getPackageDependencies(packageName, dependencies);

    expect(result).toEqual(expect.arrayContaining(['B', 'C']));
  });

  it('returns A and B and C dependencies (level 2 transitive)', () => {
    const packageName = 'A';
    const dependencies = { A: ['B'], B: ['C'], C: ['D'] };

    const result = getPackageDependencies(packageName, dependencies);

    expect(result).toEqual(expect.arrayContaining(['B', 'C', 'D']));
  });

  it('copes with transitive and circular dependencies', () => {
    const packageName = 'A';
    const dependencies = {
      A: ['B', 'C'],
      C: ['D', 'E'],
      D: ['B'],
      E: ['A']
    };

    const result = getPackageDependencies(packageName, dependencies);

    expect(result).toEqual(expect.arrayContaining(['E', 'D', 'C', 'B']));
  });
});
