const install = require('./install');
const remove = require('./remove');
const depend = require('./depend');

// Parses the input and returns an array of strings with the output
const processInput = (input) => {
  const result = [];
  const [count, ...commands] = input.split('\n');

  const state = {
    userInstalledRootPackages: [],
    installedPackages: [],
    dependencies: {}
  };

  commands.forEach((command) => {
    result.push(command);

    const [commandType, ...params] = command.split(' ');

    if (commandType === 'DEPEND') {
      const [packageId, ...packageDependencies] = params;
      let ignoreCommand = false;

      packageDependencies.forEach((packageDependency) => {
        if (state.dependencies[packageDependency]
          && state.dependencies[packageDependency].some(d => d === packageId)) {
          result.push(`${packageDependency} depends on ${packageId}, ignoring command`);
          ignoreCommand = true;
        }
      });

      if (!ignoreCommand) {
        state.dependencies = depend(packageId, packageDependencies, state.dependencies);
      }
    } else if (commandType === 'INSTALL') {
      const [packageId] = params;
      const newInstalledPackages = install(packageId, state.installedPackages, state.dependencies);

      state.installedPackages = [...state.installedPackages, ...newInstalledPackages];
      state.userInstalledRootPackages = [...state.userInstalledRootPackages, packageId];

      if (newInstalledPackages.length === 0) {
        result.push(`${packageId} is already installed`);
      }

      newInstalledPackages.forEach((installedPackage) => {
        result.push(`Installing ${installedPackage}`);
      });
    } else if (commandType === 'REMOVE') {
      const [packageId] = params;
      const { toRemovePackages, error } = remove(
        packageId, state.installedPackages, state.dependencies, state.userInstalledRootPackages
      );

      state.installedPackages = state.installedPackages
        .filter(installedPackage => !toRemovePackages.includes(installedPackage));
      state.userInstalledRootPackages = state.userInstalledRootPackages
        .filter(installedPackage => packageId !== installedPackage);

      if (error) {
        result.push(error);
      }

      toRemovePackages.forEach((toRemovePackage) => {
        result.push(`Removing ${toRemovePackage}`);
      });
    } else if (commandType === 'LIST') {
      state.installedPackages.forEach((packageId) => {
        result.push(packageId);
      });
    } else if (commandType !== 'END') {
      console.error('unrecognized command type', command);
    }
  });

  return result;
};

module.exports = processInput;
