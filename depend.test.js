const depend = require('./depend');

describe('depend', () => {
  it('adds empty array of dependencies', () => {
    const packageName = 'A';
    const packageDependencies = [];
    const oldDependencies = {};

    const result = depend(packageName, packageDependencies, oldDependencies);

    expect(result).toEqual({ A: [] });
  });

  it('adds one dependency', () => {
    const packageName = 'A';
    const packageDependencies = ['C'];
    const oldDependencies = {
      A: ['B']
    };

    const result = depend(packageName, packageDependencies, oldDependencies);

    expect(result).toEqual({ A: ['B', 'C'] });
  });

  it('adds multiple dependencies', () => {
    const packageName = 'A';
    const packageDependencies = ['C', 'D'];
    const oldDependencies = {
      A: ['B']
    };

    const result = depend(packageName, packageDependencies, oldDependencies);

    expect(result).toEqual({ A: ['B', 'C', 'D'] });
  });
});
