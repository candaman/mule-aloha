const remove = require('./remove');

describe('remove', () => {
  it('removes a single package without dependencies', () => {
    const packageName = 'A';
    const installedPackages = ['A'];
    const dependencies = {};

    const result = remove(packageName, installedPackages, dependencies, []);

    expect(result).toEqual({
      toRemovePackages: ['A']
    });
  });

  it('removes a single package and its unused dependencies', () => {
    const packageName = 'A';
    const installedPackages = ['A', 'B'];
    const dependencies = { A: ['B'] };

    const result = remove(packageName, installedPackages, dependencies, []);

    expect(result).toEqual({
      toRemovePackages: ['A', 'B']
    });
  });

  it('do not remove used dependencies', () => {
    const packageName = 'A';
    const installedPackages = ['A', 'B', 'C'];
    const dependencies = { A: ['C'], B: ['C'] };

    const result = remove(packageName, installedPackages, dependencies, []);

    expect(result).toEqual({
      toRemovePackages: ['A']
    });
  });

  it('do not remove package if its not installed', () => {
    const packageName = 'A';
    const installedPackages = [];
    const dependencies = {};

    const result = remove(packageName, installedPackages, dependencies, []);

    expect(result).toEqual({
      error: 'A is not installed',
      toRemovePackages: []
    });
  });

  it('do not remove package if still needed', () => {
    const packageName = 'B';
    const installedPackages = ['A', 'B'];
    const dependencies = { A: ['B'] };

    const result = remove(packageName, installedPackages, dependencies, []);

    expect(result).toEqual({
      error: 'B is still needed',
      toRemovePackages: []
    });
  });

  it('do not remove package if still needed nor its dependencies', () => {
    const packageName = 'B';
    const installedPackages = ['A', 'B', 'C'];
    const dependencies = { A: ['B'], B: ['C'] };

    const result = remove(packageName, installedPackages, dependencies, []);

    expect(result).toEqual({
      error: 'B is still needed',
      toRemovePackages: []
    });
  });

  it('do not remove a package if it was previously explicitly installed by the user', () => {
    const packageName = 'A';
    const installedPackages = ['A', 'B'];
    const dependencies = { A: ['B'] };
    const userInstalledRootPackages = ['B'];

    const result = remove(packageName, installedPackages, dependencies, userInstalledRootPackages);

    expect(result).toEqual({
      toRemovePackages: ['A']
    });
  });
});
