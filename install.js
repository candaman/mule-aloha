const getPackageDependencies = require('./getPackageDependencies');

// Returns an array with the packages that should be installed, included there dependencies
const install = (packageId, installedPackages, dependencies) => {
  if (installedPackages.includes(packageId)) {
    return [];
  }

  const dependenciesToInstall = getPackageDependencies(packageId, dependencies)
    .filter(dependencyPackage => !installedPackages.includes(dependencyPackage));

  return [...dependenciesToInstall, packageId];
};

module.exports = install;
