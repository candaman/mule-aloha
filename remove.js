const getPackageDependencies = require('./getPackageDependencies');

// Returns an array with the packages that should be removeed, included unused dependencies
const remove = (packageId, installedPackages, dependencies, userInstalledRootPackages) => {
  const toRemovePackages = [packageId, ...getPackageDependencies(packageId, dependencies)];

  // Do not remove any package explicitly installed by the user (nor its dependencies)
  const rootDependencies = userInstalledRootPackages
    .filter(installedPackage => installedPackage !== packageId);

  const neededDependencies = installedPackages
    .filter(installedPackage => !toRemovePackages.includes(installedPackage))
    .concat(rootDependencies)
    .map(installedPackage => getPackageDependencies(installedPackage, dependencies))
    .reduce((acc, packageDependencies) => [...acc, ...packageDependencies], [])
    .concat(rootDependencies);

  if (!installedPackages.includes(packageId)) {
    return {
      error: `${packageId} is not installed`,
      toRemovePackages: []
    };
  }

  if (neededDependencies.includes(packageId)) {
    return {
      error: `${packageId} is still needed`,
      toRemovePackages: []
    };
  }

  return {
    toRemovePackages: toRemovePackages
      .filter(toRemovePackage => !neededDependencies.includes(toRemovePackage))
  };
};

module.exports = remove;
