// Returns an array of dependencies given a package name and the dependencies dictionary
// Returns both direct and transitive dependencies in a single array
const getPackageDependencies = (packageId, dependenciesDictionary, parentPackages = []) => {
  const directDependencies = dependenciesDictionary[packageId] || [];

  if (parentPackages.includes(packageId)) {
    return directDependencies;
  }

  const transitiveDependencies = directDependencies
    .map((dependentPackage) => {
      if (parentPackages.includes(dependentPackage)) {
        return [];
      }
      return getPackageDependencies(
        dependentPackage, dependenciesDictionary, [...parentPackages, packageId]
      );
    })
    .reduce((dependenciesAcc, someDepencies) => [...dependenciesAcc, ...someDepencies], []);

  return [...directDependencies, ...transitiveDependencies]
    .filter(dependency => !parentPackages.includes(dependency));
};

module.exports = getPackageDependencies;
