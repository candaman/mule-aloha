// Adds the new dependencies to the dictionary of dependencies and returns the dictionary
const depend = (packageId, newPackageDependencies, dependencies) => {
  const oldPackageDependencies = (dependencies[packageId] || []);

  return {
    ...dependencies,
    [packageId]: [...oldPackageDependencies, ...newPackageDependencies]
  };
};

module.exports = depend;
