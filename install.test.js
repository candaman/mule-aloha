const install = require('./install');

describe('install', () => {
  it('installs a single package without dependencies', () => {
    const packageName = 'A';
    const installedPackages = [];
    const dependencies = {};

    const result = install(packageName, installedPackages, dependencies);

    expect(result).toEqual(['A']);
  });

  it('does not install a package if it is already installed', () => {
    const packageName = 'A';
    const installedPackages = ['A'];
    const dependencies = {};

    const result = install(packageName, installedPackages, dependencies);

    expect(result).toEqual([]);
  });

  it('does not install a dependency if it is already installed', () => {
    const packageName = 'A';
    const installedPackages = ['B'];
    const dependencies = { A: ['B'] };

    const result = install(packageName, installedPackages, dependencies);

    expect(result).toEqual(['A']);
  });

  it('installs a package with dependencies', () => {
    const packageName = 'A';
    const installedPackages = [];
    const dependencies = { A: ['B'] };

    const result = install(packageName, installedPackages, dependencies);

    expect(result).toEqual(expect.arrayContaining(['A', 'B']));
  });

  it('installs a package with transitive dependencies', () => {
    const packageName = 'A';
    const installedPackages = [];
    const dependencies = { A: ['B'], B: ['C'] };

    const result = install(packageName, installedPackages, dependencies);

    expect(result).toEqual(expect.arrayContaining(['A', 'B', 'C']));
  });
});
